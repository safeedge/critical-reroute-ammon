#!/bin/sh

conntrack -F

ip6tables -t mangle -F

nft delete table ip6 sr
nft delete table ip6 nlsdn

for pref in `ip -6 rule | grep -vE 'local|main' | grep lookup | cut -d: -f1` ; do
    ip -6 rule del pref $pref
    ip -6 route flush table $pref
done

for pref in 4096 4097 4098 4099 4100 4101 4102 4103 4104 4105 4106 4107 4108 4109 ; do
    ip -6 route flush table $pref
done

systemctl stop nlsdn
rm -f /var/lib/nlsdn/db.json
systemctl start nlsdn
