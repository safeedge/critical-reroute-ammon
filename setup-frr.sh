#!/bin/sh

set -x

SRC=`dirname $0`
. $SRC/setup-lib.sh

# Exit if we've already done this.
if [ -e $OURDIR/frr-done ]; then
    exit 0
fi

cd $OURDIR

curl -sL https://repos.emulab.net/emulab.key | apt-key add -
(. /etc/os-release && echo "deb https://repos.emulab.net/frr/${ID} ${UBUNTU_CODENAME} main" > /etc/apt/sources.list.d/frr.list)
apt-get -y update
maybe_install_packages frr frr-pythontools frr-snmp frr-rpki-rtrlib

if [ -f $OURDIR/mgmt-ip -a -f $OURDIR/interfaces-ipv6 ]; then
    MGMTIP=`cat $OURDIR/mgmt-ip`
    cat <<EOF >/etc/frr/zebra.conf
hostname $NODEID
password zebra
enable password zebra
log file /var/log/frr/zebra.log
ip forwarding
line vty

EOF
    while read line ; do
	iface=`echo $line | cut -d, -f1`
	addr=`echo $line | cut -d, -f2`
	echo "interface $iface" >> /etc/frr/zebra.conf
	echo "  ipv6 address $addr" >> /etc/frr/zebra.conf
    done < $OURDIR/interfaces-ipv6

    cat <<EOF >/etc/frr/ospf6d.conf
hostname $NODEID
password zebra
log file /var/log/frr/ospf6d.log
service advanced-vty

EOF
    while read line ; do
	iface=`echo $line | cut -d, -f1`
	echo "interface $iface" >> /etc/frr/ospf6d.conf
    done < $OURDIR/interfaces-ipv6
    cat <<EOF >>/etc/frr/ospf6d.conf

router ospf6
  ospf6 router-id $MGMTIP
EOF
    while read line ; do
	iface=`echo $line | cut -d, -f1`
	addr=`echo $line | cut -d, -f2`
	cat <<EOF >>/etc/frr/ospf6d.conf
  area 0.0.0.0 range $addr
  interface $iface area 0.0.0.0
EOF
    done < $OURDIR/interfaces-ipv6

    cat <<EOF >>/etc/frr/daemons

ospf6d=yes
EOF
fi

# Daemons look for this in preference to /etc/frr/${daemon}.conf,
# and it exists by default on Ubuntu, thus overrides everything.
# Make sure it does not exist before we run anything.
rm -f /etc/frr/frr.conf

systemctl enable frr
systemctl restart frr

touch $OURDIR/frr-done

exit 0
